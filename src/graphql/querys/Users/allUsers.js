import {
    GraphQLList
} from 'graphql';
import Users from '../../../models/Users';
import {listUser} from '../../types/Users'

const queryAllUsers = {
    type: new GraphQLList(listUser),
    resolve(){
        return Users.find().then((lou)=>{
            return lou;
        }).catch((err)=>{
            throw new Error("The users collection couldn´t be reached");
        })
    }
};

export default queryAllUsers;