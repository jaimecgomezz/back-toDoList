import userQuery from './Users';
import taskListQuery from './TaskList';
export default{
    ...userQuery,
    ...taskListQuery,
}