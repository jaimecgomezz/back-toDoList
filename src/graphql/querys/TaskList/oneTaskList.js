import {
    GraphQLNonNull,
    GraphQLID,
    GraphQLList
} from 'graphql';
import TaskList from '../../../models/TaskList';
import {taskList} from '../../types/TaskList'

const queryOneTaskList = {
    type: GraphQLList(taskList) ,
    args:{
        id:{
            name: "ID",
            type: GraphQLNonNull(GraphQLID)
        }
    },
    resolve(root, params){
        return TaskList.find({"userId":params.id}).then((tlf)=>{
            return tlf;
        }).catch((err)=>{
            throw new Error("Your task list couldn´t be reached");
        })
    }
};

export default queryOneTaskList;