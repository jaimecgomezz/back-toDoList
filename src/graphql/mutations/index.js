import TLMutations from './TaskList';
import UMutations from './Users';
export default{
    ...TLMutations,
    ...UMutations
}