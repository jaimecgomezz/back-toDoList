import{
    GraphQLNonNull,
    GraphQLID
} from 'graphql'
import UsersSchema from '../../../models/Users'
import TaskListSchema from '../../../models/TaskList';
import {taskList, taskListInput} from '../../types/TaskList'

const deleteTaskList={
    type:taskList,
    args:{
        id:{
            name: "ID",
            type: new GraphQLNonNull(GraphQLID)
        },
        data:{
            name:"Data",
            type: taskListInput
        }
    },
    resolve(root, params, context){
        return TaskListSchema.findByIdAndRemove(params.id).then((tlr)=>{
            return UsersSchema.findByIdAndUpdate(params.data.userId, {$pull:{"taskLists":params.id}}).then(()=>{
                return tlr;
            }).catch((err)=>{
                throw new Error("Your task list has been updated but it might still be shown");
            });
        }).catch((err)=>{
            throw new Error("Your task list couldn´t be removed")
        });
    }
}

export default deleteTaskList;