import addTL from './add';
import removeTL from './remove';
import addChores from './addChores';
import deleteChore from './deleteChore';
export default{
    addTL,
    removeTL,
    addChores,
    deleteChore
}