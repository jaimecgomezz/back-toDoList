import {
    GraphQLNonNull,
    GraphQLID
} from 'graphql'
import {taskList, taskListInput} from '../../types/TaskList'
import TaskListSchema from '../../../models/TaskList'
import UsersSchema from '../../../models/Users'

const addTaskList = {
    type: taskList,
    args:{
        data:{
            name:"data",
            type: new GraphQLNonNull(taskListInput)
        }
    },
    resolve(root, params){
        let tl = TaskListSchema(params.data);
        let tC = tl.save();
        return tC.then((created)=>{
            return UsersSchema.findByIdAndUpdate(params.data.userId, {$push:{"taskLists":created._id}}).then((updated)=>{
                return created; 
            }).catch((err)=>{
                console.log(err)
                throw new Error("Your list has been created but won´t be reached");
            })
        }).catch((err)=>{
            console.log(err)
            throw new Error("Your list hasn´t been created");
        });
    }
};

export default addTaskList;