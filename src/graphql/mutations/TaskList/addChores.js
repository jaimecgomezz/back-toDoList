import {
    GraphQLNonNull,
    GraphQLID
} from 'graphql';
import TaskListSchema from '../../../models/TaskList';
import {taskList, taskListInput} from '../../types/TaskList';

const addChores = {
    type: taskList,
    args:{
        id:{
            name:"ID",
            type: GraphQLNonNull(GraphQLID)
        },
        data:{
            name:"Data",
            type: GraphQLNonNull(taskListInput)             
        }
    },
    resolve(root, params){
        return TaskListSchema.findByIdAndUpdate(params.id, {$push:{"chores":{...params.data.chores}}}).then((updated)=>{
            return TaskListSchema.findById(updated._id).then((tlf)=>{
                return tlf;
            }).catch((err)=>{
                console.log(err);
                throw new Error("Your task list has been updated but couldn´t be reached");
            })
        }).catch((err)=>{
            console.log(err);
            throw new Error("Your schore couldn´t been added");
        })
    }
};

export default addChores;