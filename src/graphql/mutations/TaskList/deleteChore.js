import {
    GraphQLID,
    GraphQLString
} from 'graphql';
import {taskList} from '../../types/TaskList'
import TaskListSchema from '../../../models/TaskList'

const deteleChore = {
    type:taskList,
    args:{
        id:{
            name: "ID",
            type: GraphQLID
        },
        choreName:{
            name:"Name",
            type: GraphQLString
        }
    },
    resolve(root, params){
        return TaskListSchema.findByIdAndUpdate(params.id, {$pull:{"chores":{"choreName":params.choreName}}}).then((updated)=>{
            return TaskListSchema.findById(updated._id).then((final)=>{
                return final;
            }).catch((err)=>{
                throw new Error("Your chore has been removed but couldnt be reached");
            })
        }).catch((err)=>{
            throw new Error("Your chore hasn´t been removed");
        })
    }
};

export default deteleChore;