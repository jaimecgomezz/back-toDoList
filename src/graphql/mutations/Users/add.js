import {
    GraphQLNonNull
} from 'graphql'
import UserSchema from '../../../models/Users';
import {listUser, inputUsers} from '../../types/Users'

const addUser = {
    type: listUser,
    args:{
        data:{
            name:"Name",
            type: GraphQLNonNull(inputUsers)
        }
    },
    resolve(root, params){
        let uc = UserSchema(params.data);
        let uf = uc.save();
        if(!uf) throw new Error("Your user couldn´t be created");
        return uf;
    }
}

export default addUser;