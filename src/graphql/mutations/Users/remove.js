import {
    GraphQLNonNull,
    GraphQLID
} from 'graphql';
import TaskListSchema from '../../../models/TaskList';
import UserSchema from '../../../models/Users';
import {listUser} from '../../types/Users';

const removeUser = {
    type: listUser,
    args:{
        id:{
            type: GraphQLNonNull(GraphQLID)
        },
        data:{
            type: GraphQLID
        }
    },
    resolve(root, params){
        return UserSchema.findByIdAndRemove(params.id).then((ud)=>{
            return TaskListSchema.deleteMany({"userId":params.id}).then(()=>{
                return ud;
            }).catch((err)=>{
                throw new Error("Your user has been deleted but their Task Lists may still appear");
            });
        }).catch((err)=>{
            throw new Error("Your user couldn´t be removed");
        })
    }
};

export default removeUser;