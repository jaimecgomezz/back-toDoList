import {
    GraphQLObjectType,
    GraphQLInputObjectType,
    GraphQLID,
    GraphQLNonNull,
    GraphQLBoolean,
    GraphQLString,
    GraphQLList
} from 'graphql';

export const listUser = new GraphQLObjectType({
    name:"listUsers",
    fields: ()=>({
        _id:{
            type: GraphQLNonNull(GraphQLID)
        },
        name:{
            type: GraphQLNonNull(GraphQLString)
        },
        isActive:{
            type: GraphQLBoolean
        },
        taskLists:{
            type: GraphQLList(GraphQLID)
        },
        createdAt:{
            type: GraphQLString
        }
    })
});

export const inputUsers = new GraphQLInputObjectType({
    name:"inputUser",
    fields: ()=>({
        name:{
            type: GraphQLNonNull(GraphQLString)
        },
        isActive:{
            type: GraphQLBoolean
        },
        taskLists:{
            type: GraphQLList(GraphQLID)
        }
    })
});


 