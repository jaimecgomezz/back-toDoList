import {
    GraphQLObjectType,
    GraphQLInputObjectType,
    GraphQLString, 
    GraphQLBoolean, 
    GraphQLID, 
    GraphQLList,
} from 'graphql'

export const taskList = new GraphQLObjectType({
    name:"taskLists",
    fields: ()=>({
        _id:{
            type: GraphQLID,
        },
        listName:{
            type: GraphQLString,
        },
        chores: {type:GraphQLList(new GraphQLObjectType({
            name:"chores",
            fields: ()=>({
                choreName:{
                    type: GraphQLString
                },
                done:{
                    type: GraphQLBoolean
                }
            })
        }))},
        createdAt:{
            type: GraphQLString
        },
        isActive:{
            type: GraphQLBoolean
        }
    })
})

export const taskListInput = new GraphQLInputObjectType({
    name: "taskListInputs",
    fields: ()=>({
        userId:{
            type: GraphQLID,
        },
        listName:{
            type: GraphQLString
        },
        chores:{ type:new GraphQLInputObjectType({
            name: "choresInput",
            fields: ()=>({
                choreName:{
                    type: GraphQLString
                },
                done:{
                    type: GraphQLBoolean
                }
            })
        })},
        isActive:{
            type: GraphQLBoolean
        }
    })
})