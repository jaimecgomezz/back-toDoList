import {
    GraphQLObjectType,
    GraphQLSchema,
} from 'graphql';
import Query from './querys';
import Mutation from './mutations'

export default new GraphQLSchema({
    query: new GraphQLObjectType({
        name: "Query",
        fields: Query
    }),
    mutation: new GraphQLObjectType({
        name: "Mutations",
        fields: Mutation
    })
});