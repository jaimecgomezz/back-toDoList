const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name:{
        type: String,
        required: true
    },
    isActive:{
        type: Boolean,
        default: true,
    },
    taskLists:[{
        type: Schema.Types.ObjectId,
        ref:"TaskList"
    }],
    createdAt:{
        type: Date,
        default: new Date()
    }
}, {collection:"Users", timestamps:true});

export default mongoose.model('Users', UserSchema);