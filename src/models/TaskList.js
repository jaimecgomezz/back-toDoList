const mongoose = require("mongoose");
const Schema = mongoose.Schema;
import Users from './Users'

const TaskListSchema = new Schema({
    "userId":{
        type: Schema.Types.ObjectId,
        ref: "Users"
    },
    "listName":{
        type: String,
    },
    "chores":[{
        "choreName":{
            type:String
        },
        "done":Boolean
    }],
    "createdAt":{
        type: Date,
        default: new Date()
    },
    "isActive":{
        type: Boolean,
        default: true,

    },
}, {collection:"TaskList", timestamps:true})

TaskListSchema.pre('save', function(){
    let tasklist = this;
    Users.findByIdAndUpdate(tasklist.userId, {$push:{"taskLists": taskList._id}});
})

export default mongoose.model('TaskList', TaskListSchema)