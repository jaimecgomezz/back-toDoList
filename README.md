# To-do-list server

Aquí se encuentran los componentes necesarios para correr el back-end que atiende y procesa las peticiones del front-end
del proyecto. A grandes rasgos, la mecánica es la siguiente:   

1.Se recibe la solicitud del lado del ciente mediante el server, levantado con express.

2.La consulta llega a graphql y la interpreta, realizando un query directamente a la base de datos, o bien, realizando un mutación 
y posterirmente un query del elementi mutado.  

3.Exitosa la consulta devuelve la respuesta a al cliente.

## Primeros pasos

Para comenzar dembemos clonar la carpeta, ya sea desde este repositorio o mediante un fork al propio.

### Requisitos

Es necesario tener instalado un manejador de paquetes, ya sea Yarn, NPM, Bower u otros. 
En caso de usar un sistema operativo Linux podemos acceder a la terminal, sutuarnos en la ruta donde deseemos 
clonar la carpeta y ejecutar el comando 
`git clone https://github.com/JaimeCGomez/back-toDoList.git`.
En caso de usar Windows, habrá que descargar git, acceder a su terminal -git bash- y correr el mismo comando.

### Instalación

Habiendo clonado la carpeta exitosamente, accedemos a ella mediante terminal, y nos situamos al nivel del package.json
Para este ejemplo, se utiliza npm. El comando es el siguiente:
``` 
$ npm i
```

Una vez concluya la instalación de las dependencias necesarias, corremos en terminal el siguiente comando:

```
$ npm run server
```

Este comando utiliza a nodemon para levantar el servidor en el puerto 5000, así como a babel para 
transpilar el código, escrito en su mayor parte con la sitaxis de ECMAScript 6, a JavaScript "puro". 
Dicho comando es funcional solo en desarrollo.
Una vez que en la terminal aparezcan los siguientes fragmentos, estará listo para atender al cliente:

```
$ Working on port 5000
$ Already connected to database
```

En caso de querer llevarlo aproducción podemos ejecutar el siguiente comando:
```
$ npm run build
```
Se creará una carpeta build que podremos utilizar para llevar a producción.

## Construido con:

* [Express](https://github.com/expressjs/express) - El framework de NodeJS utilizado para levantar el servidor.
* [GraphQL](https://graphql.org/learn/) - Lenguaje de consultas
* [Mongoose](https://rometools.github.io/rome/) - Lenguaje de modelado para MongoDB
* [Mlab](https://mlab.com/) - Database-as-a-service utilizado para almacenar la base de datos.

## Autores

* **Jaime Iván Castro Gómez** 

