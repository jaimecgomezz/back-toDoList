const express = require("express");
const app = express();
const bodyParser = require("body-parser")
const cors = require("cors")
const graphqlHTTP = require("express-graphql")
const mongoose = require("mongoose");

import schema from './src/graphql/index'

const PORT = process.env.PORT || 5000;

app.use(bodyParser.json());
app.use(cors());

mongoose.connect("mongodb://JaimeCGomez:abc123@ds145911.mlab.com:45911/to_do_list_database", {useNewUrlParser:true});
let db = mongoose.connection;
db.on('error', ()=>{
    console.log("Couldn´t connect to database");
}).once('open',()=>{
    console.log("Already connected to database")
})

app.use('/graphql', graphqlHTTP((req, res)=>({
    schema,
    graphiql:true,
    pretty:true,
})))

app.get('/', (req,res)=>{
    res.send("Already working");
});

app.listen(PORT, ()=>{
    console.log("Working on port", PORT)
})